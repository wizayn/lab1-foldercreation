﻿using UnityEngine;
using System.Collections;
using UnityEditor;



public class FolderCreationScript : MonoBehaviour 
{
	//Add a toolbar to the editor in unity
	//create folders
	//Add text documentation to explain folder structure

	[MenuItem("Tool Creation/Create folder")]

	public static void test()
	{
		//Create a folder named Materials in the Assets folder
		AssetDatabase.CreateFolder("Assets", "Materials");
		//create a file name "folderStructure.txt" in the Assets/Textures folder
		System.IO.File.WriteAllText(Application.dataPath + "/Materials/folderStructure.txt", "this folder is for storing materials");

		//Create a folder Named Textures in the Assets folder
		AssetDatabase.CreateFolder("Assets", "Textures");
		//Create a folder named Textures in the assets folder
		System.IO.File.WriteAllText(Application.dataPath + "/Textures/folderStructure.txt", "this folder is for storing textures");

		//Create a folder Named Textures in the Assets folder
		AssetDatabase.CreateFolder("Assets", "Prefabs");
		//Create a file name folderStructure.txt" in the Assets/Textures folder
		System.IO.File.WriteAllText(Application.dataPath + "/Prefabs/folderStructure.txt", "this folder is for storing prefabs");
			
		//Create a folder Named Scripts in the Assets folder
		AssetDatabase.CreateFolder("Assets", "Scripts");
		//Create a file named "folderStructure.txt" in the Assets/Scripts folder
		System.IO.File.WriteAllText(Application.dataPath + "/Scripts/folderStructure.txt", "this folder is for storing scripts");

		//Create a folder Named Scripts in the Assets folder
		AssetDatabase.CreateFolder("Assets", "Scenes");
		//Create a file named "folderStructure.txt" in the Assets/Scenes folder
		System.IO.File.WriteAllText(Application.dataPath + "/Scenes/folderStructure,txt", "this folder is for storing Scenes"); 
			
		//Create a folder Named Animations in the Assets folder
		AssetDatabase.CreateFolder("Assets", "Animations");
		//Create a file named "FolderStructure.txt" in the Assets/Animations folder
		System.IO.File.WriteAllText(Application.dataPath + "/Animations/folderStructure.txt", "This folder is for storing raw Animations");

		//Create a folder Named AnimationControllers in the Animation folder
		AssetDatabase.CreateFolder ("Assets/Animations", "AnimationControllers");
		//Create a file named"folderStructure.txt" in the Assets/Animations/AnimationControllers folder");
		System.IO.File.WriteAllText(Application.dataPath + "/Animations/AnimationControllers/folderStructure.txt", "This folder is For storing Animation Controllers");


	
		//Where to write the file
		//string fileLocation = Application.dataPath;

		//System.IO.File.WriteAllText
		//System.IO.File.WriteAllText(fileLocation + "/help.txt", "Please place all raw texture in the Textures folder.\nCreate materials in the Materials folder.\n " +
		//"Prefabs should go into the Prefacs folder. Create a new folder inside of refabs for each scene the prefabs will reside in.\n" + 
		//"Scripts should go into the scripts folder. Create a new folder inside of Scripts that share a common function.\n" +
		//"Scenes should be saved into the Scenes folder. Create a new folder inside scenes for scenes that share a common function, sauch as Menu scenes\n" +
		//"Animation clips belong inside the animations FolderCreationScript. Controlllers created from those clips should go into the Animation Controllers folder." );


		//Assets/help.txt
		AssetDatabase.Refresh();

	}
}
