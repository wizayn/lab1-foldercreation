﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class OYOFolderCreationScript : MonoBehaviour {

	[MenuItem("Tool Creation/ OYO Create folder")]

	public static void CreateFolders()
	{
		AssetDatabase.CreateFolder ("Assets", "Dynamic Assets");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets", "Resources");
		System.IO.File.WriteAllText (Application.dataPath + "/Dynamic Assets/Resources/folderStructure.txt", "This folder is for storing resources");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources", "Animations");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources/Animations", "Sources");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources", "Animation Controllers");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources", "Effects");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources", "Models");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources/Models", "Character");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources/Models", "Environment");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources", "Prefabs");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources/Prefabs", "Common");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources", "Sounds");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources/Sounds", "Music");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources/Sounds/Music", "Common");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources/Sounds", "SFX");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources/Sounds/SFX", "Common");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources", "Textures");

		AssetDatabase.CreateFolder ("Assets/Dynamic Assets/Resources/Textures", "Common");

		System.IO.File.WriteAllText (Application.dataPath + "/Editor/folderStructure.txt", "This folder is for storing Editors");

		AssetDatabase.CreateFolder ("Assets", "Extensions");
		System.IO.File.WriteAllText(Application.dataPath + "/Extensions/folderStructure", "This folder is for storing extensions");

		AssetDatabase.CreateFolder ("Assets", "Gizmos");
		System.IO.File.WriteAllText(Application.dataPath + "/Gizmos/folderStructure.txt", "This folder is for storing Gizmos");

		AssetDatabase.CreateFolder ("Assets", "Plugins");
		System.IO.File.WriteAllText (Application.dataPath + "/Plugins/folderStructure.txt", "This folder is for storing plugins");

		AssetDatabase.CreateFolder ("Assets", "Scripts");
		System.IO.File.WriteAllText(Application.dataPath + "/Scripts/folderStructure.txt", "This folder is for storing Scripts");

		AssetDatabase.CreateFolder("Assets/Scripts", "Common");

		AssetDatabase.CreateFolder ("Assets", "Shaders");
		System.IO.File.WriteAllText(Application.dataPath + "/Shaders/folderStructure.txt", "This folder is for storing shaders");

		AssetDatabase.CreateFolder ("Assets", "Static Assets");

		AssetDatabase.CreateFolder ("Assets/Static Assets", "Animations");

		AssetDatabase.CreateFolder ("Assets/Static Assets/Animations", "Sources");

		AssetDatabase.CreateFolder ("Assets/Static Assets", "Animation Controllers");

		AssetDatabase.CreateFolder ("Assets/Static Assets", "Effects");

		AssetDatabase.CreateFolder ("Assets/Static Assets", "Models");

		AssetDatabase.CreateFolder ("Assets/Static Assets/Models", "Character");

		AssetDatabase.CreateFolder ("Assets/Static Assets/Models", "Enviornment");

		AssetDatabase.CreateFolder ("Assets/Static Assets", "Prefabs");

		AssetDatabase.CreateFolder ("Assets/Static Assets/Prefabs", "Common");

		AssetDatabase.CreateFolder ("Assets/Static Assets", "Scenes");

		AssetDatabase.CreateFolder ("Assets/Static Assets", "Sounds");

		AssetDatabase.CreateFolder ("Assets/Static Assets/Sounds", "Music");

		AssetDatabase.CreateFolder ("Assets/Static Assets/Sounds/Music", "Common");

		AssetDatabase.CreateFolder ("Assets/Static Assets/Sounds", "SFX");

		AssetDatabase.CreateFolder ("Assets/Static Assets/Sounds/SFX", "Common");

		AssetDatabase.CreateFolder ("Assets/Static Assets", "Textures");

		AssetDatabase.CreateFolder ("Assets/Static Assets/Textures", "Common");

		System.IO.File.WriteAllText (Application.dataPath + "/Static Assets/folderStructure.txt", "This folder is for storing static assets");

		AssetDatabase.CreateFolder ("Assets", "Testing");


	}
}
